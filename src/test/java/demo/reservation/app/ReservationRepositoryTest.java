package demo.reservation.app;

import static org.assertj.core.api.Java6Assertions.assertThat;

import demo.reservation.app.reservation.Reservation;
import demo.reservation.app.reservation.ReservationRepository;
import demo.reservation.app.security.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author aklemanovits
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class ReservationRepositoryTest {

	@Autowired
	private TestEntityManager em;

	@Autowired
	private ReservationRepository reservationRepository;

	@Test
	public void save() {
		//GIVEN
		User feri = em.persistAndFlush(new User("Feri", "valami","valami2"));

		Reservation reservation = new Reservation("alma");
		reservation.setUser(feri);

		//WHEN
		Reservation saved = em.persistAndFlush(reservation);

		//THEN
		assertThat(saved.getId()).isNotNull();
		System.err.println(saved);
	}
}