package demo.reservation.app.view;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;

import demo.reservation.app.reservation.Reservation;
import demo.reservation.app.reservation.ReservationService;

/**
 * @author aklemanovits
 */
@SpringView(name = MainView.VIEW_NAME)
public class MainView extends VerticalLayout implements View {

	public static final String VIEW_NAME = "main";

	private final ReservationService reservationService;
	private final Grid<Reservation> grid;

	public MainView(ReservationService reservationService) {
		this.reservationService = reservationService;
		this.grid = new Grid<>();

		setSizeFull();

		initGrid();
	}

	private void initGrid() {
		grid.addColumn(Reservation::getId).setCaption("Id");
		grid.addColumn(Reservation::getName).setCaption("Name");
		grid.setDataProvider(
				(list, i, i1) -> reservationService.findAll().stream(),
				() -> reservationService.findAll().size());

		addComponent(grid);
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {
		grid.getDataProvider().refreshAll();
	}
}
