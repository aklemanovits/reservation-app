package demo.reservation.app.view;

import com.vaadin.annotations.Push;
import com.vaadin.navigator.PushStateNavigation;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.ui.Transport;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringNavigator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import demo.reservation.app.security.MyUserDetails;
import demo.reservation.app.security.User;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author aklemanovits
 */
@SpringUI
@PushStateNavigation
@Push(transport = Transport.WEBSOCKET_XHR)
public class ReservationUI extends UI {

	private final SpringNavigator navigator;

	private User user;

	public ReservationUI(SpringNavigator navigator) {
		this.navigator = navigator;
	}

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		MyUserDetails principal = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = principal.getUser();

		VerticalLayout content = new VerticalLayout();
		content.setMargin(false);
		content.setSpacing(false);
		content.setSizeFull();
		setContent(content);

		content.addComponent(new Label("Welcome: " + user.getName()));

		Button logout = new Button("Logout");
		logout.addClickListener(click -> {
			getUI().getSession().close();
			getUI().getPage().setLocation("/logout");
		});
		content.addComponent(logout);

		MenuBar menuBar = new MenuBar();
		menuBar.addItem(MainView.VIEW_NAME, item -> getNavigator().navigateTo(MainView.VIEW_NAME));
		menuBar.addItem(AdminView.VIEW_NAME, item -> getNavigator().navigateTo(AdminView.VIEW_NAME));
		content.addComponent(menuBar);

		Panel viewContainer = new Panel();
		viewContainer.setSizeFull();
		content.addComponent(viewContainer);
		content.setExpandRatio(viewContainer, 1f);

		navigator.init(this, viewContainer);
		setNavigator(navigator);

		getNavigator().navigateTo(MainView.VIEW_NAME);
	}

	public User getUser() {
		return user;
	}
}
