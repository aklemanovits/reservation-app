package demo.reservation.app.view;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

/**
 * @author aklemanovits
 */
@SpringView(name = AdminView.VIEW_NAME)
public class AdminView extends VerticalLayout implements View {

	public static final String VIEW_NAME = "admin";

	public AdminView() {
		setSizeFull();
		addComponent(new Label("This the top secret admin area!"));
	}
}
