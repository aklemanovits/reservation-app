package demo.reservation.app.config;

import static org.slf4j.LoggerFactory.getLogger;

import java.util.stream.Stream;

import demo.reservation.app.reservation.DaoReservationService;
import demo.reservation.app.reservation.Reservation;
import demo.reservation.app.reservation.ReservationRepository;
import demo.reservation.app.reservation.ReservationService;
import demo.reservation.app.security.MyUserDetailsService;
import demo.reservation.app.security.User;
import demo.reservation.app.security.UserRepository;
import org.slf4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author aklemanovits
 */
@Configuration
public class ApplicationConfiguration {

	private static final Logger LOGGER = getLogger(ApplicationConfiguration.class);

	@Bean
	public ReservationService reservationService(ReservationRepository reservationRepository) {
		return new DaoReservationService(reservationRepository);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}

	@Bean
	public UserDetailsService userDetailsService(UserRepository repository) {
		return new MyUserDetailsService(repository);
	}

	@Bean
	public CommandLineRunner dbInit(ReservationRepository reservationRepository, UserRepository userRepository, PasswordEncoder passwordEncoder) {
		return args -> {
			if ( reservationRepository.findAll().isEmpty() ) {
				LOGGER.info("Reservation table is empty, initializing with default data.");
				Stream.of("A", "B", "C", "D", "E", "F")
					  .map(Reservation::new)
					  .forEach(reservationRepository::save);
			}

			if ( userRepository.findAll().isEmpty() ) {
				userRepository.save(new User("admin", passwordEncoder.encode("admin"), "ROLE_ADMIN"));
				userRepository.save(new User("user", passwordEncoder.encode("user"), "ROLE_USER"));
			}

			LOGGER.info("users: {}", userRepository.findAll());
			LOGGER.info("reservations: {}", reservationRepository.findAll());
		};
	}
}
