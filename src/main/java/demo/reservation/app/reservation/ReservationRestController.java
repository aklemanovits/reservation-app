package demo.reservation.app.reservation;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author aklemanovits
 */
@RestController
public class ReservationRestController {

	private final ReservationService service;

	public ReservationRestController(ReservationService service) {
		this.service = service;
	}

	@GetMapping("/reservation")
	public List<Reservation> getAll() {
		return service.findAll();
	}

}
