package demo.reservation.app.reservation;

import java.util.Objects;
import java.util.StringJoiner;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import demo.reservation.app.security.User;

/**
 * @author aklemanovits
 */
@Entity
public class Reservation {

	@Id
	@GeneratedValue
	private Long id;

	private String name;

	@OneToOne
	private User user;

	public Reservation() {
	}

	public Reservation(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public boolean equals(Object o) {
		if ( this == o ) return true;
		if ( !(o instanceof Reservation) ) return false;
		Reservation that = (Reservation) o;
		return id == that.id &&
				Objects.equals(name, that.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name);
	}

	@Override
	public String toString() {
		return new StringJoiner(", ", "[", "]")
				.add("id=" + id)
				.add("name='" + name + "'")
				.toString();
	}
}
