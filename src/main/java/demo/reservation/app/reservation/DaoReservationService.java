package demo.reservation.app.reservation;

import static org.slf4j.LoggerFactory.getLogger;

import java.util.List;

import org.slf4j.Logger;

/**
 * @author aklemanovits
 */
public class DaoReservationService implements ReservationService {

	private static final Logger LOGGER = getLogger(DaoReservationService.class);

	private final ReservationRepository reservationRepository;

	public DaoReservationService(ReservationRepository reservationRepository) {
		this.reservationRepository = reservationRepository;
	}

	@Override
	public List<Reservation> findAll() {
		LOGGER.trace("findAll called");

		return reservationRepository.findAll();
	}

	@Override
	public void add(Reservation reservation) {
		reservationRepository.save(reservation);
	}
}
