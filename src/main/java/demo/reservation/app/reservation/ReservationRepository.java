package demo.reservation.app.reservation;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author aklemanovits
 */
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

	Optional<Reservation> findByName(String name);
}
