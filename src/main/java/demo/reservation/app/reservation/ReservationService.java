package demo.reservation.app.reservation;

import java.util.List;

/**
 * @author aklemanovits
 */
public interface ReservationService {

	List<Reservation> findAll();

	void add(Reservation reservation);
}
