package demo.reservation.app.security;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author aklemanovits
 */
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByName(String name);
}
