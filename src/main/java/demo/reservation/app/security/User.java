package demo.reservation.app.security;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

/**
 * @author aklemanovits
 */
@Data
@Entity
public class User {

	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private String password;
	private String authority;

	public User() {
	}

	public User(String name, String password, String authority) {
		this.name = name;
		this.password = password;
		this.authority = authority;
	}
}
